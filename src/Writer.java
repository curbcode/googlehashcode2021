import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class Writer {

    public static void write_data(String fileName, City city) {
        try {
            FileWriter myWriter = new FileWriter("out/" + fileName);

            String output = "";
            int cpt = 0;
            for (int i = 0; i < city.getIntersections().size(); i++){
                Intersection intersection = city.getIntersections().get(i);
                List<TrafficLight> trafficLights = intersection.getTrafficLights();
                List<TrafficLight> usedTrafficLights = trafficLights.stream().filter(t -> t.getNbCars() > 0).collect(Collectors.toList());

                //int poidsTotal = usedTrafficLights.stream().reduce(0, (s, e) -> s + e.getNbCars(), Integer::sum);
                if (usedTrafficLights.isEmpty()) {
                    continue;
                }
                cpt++;
//                int poidsScoreMax = usedTrafficLights
//                        .stream()
//                        .mapToInt(TrafficLight::getScoreCars)
//                        .max().orElseThrow(NoSuchElementException::new);

                int poidsMax = usedTrafficLights
                        .stream()
                        .mapToInt(TrafficLight::getPoids)
                        .max().orElseThrow(NoSuchElementException::new);

                output += String.valueOf(intersection.getId());
                output += "\n";
                output += String.valueOf(usedTrafficLights.size());
                output += "\n";

                for (int j = 0; j < usedTrafficLights.size(); j++){
                    TrafficLight trafficLight =  usedTrafficLights.get(j);
                    output += trafficLight.getStreetName() + " " + calculatePoidsTraffic(poidsMax, trafficLight.getPoids(), trafficLight.getScoreCars(), city.getNbSeconds());
                    output += "\n";
                }

            }

           myWriter.write(String.valueOf(cpt));
           myWriter.write("\n");
            myWriter.write(output);

            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private static String calculatePoidsTraffic(int poidsMax, int poids, int nbScoreCars, Integer nbSeconds) {
        Integer poidsTrafficLights = poidsMax/poids;
        // System.out.println("[poids; poidsMax] = [" + poids + "; " + poidsMax + "]");
        if (poidsTrafficLights > nbSeconds) {
            poidsTrafficLights = nbSeconds;
        }
        if (poidsTrafficLights == 0) {
            poidsTrafficLights = 1;
        }
        return String.valueOf(poidsTrafficLights);
    }


}
