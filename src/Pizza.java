import java.util.ArrayList;
import java.util.List;

public class Pizza {
    private int index;
    private List<String> ingredients;

    public Pizza(String line) {
        String[] words = line.split(" ");
        this.index = Integer.parseInt(words[0]);
        this.ingredients = new ArrayList<>();
        for (int i =1; i<words.length; i++) {
           ingredients.add(words[i].replace(",", ""));
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public List<String> getIngredients() {
        return ingredients;
    }
}
