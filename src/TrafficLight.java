public class TrafficLight {

    private int id;
    private String streetName;
    private Boolean status;
    private int timeToWait;
    private int scoreCars;
    private int nbCars;

    public TrafficLight(int id, String streetName, Boolean status) {
        this.id = id;
        this.streetName = streetName;
        this.status = status;
        this.timeToWait = 1;
        this.scoreCars = 0;
        this.nbCars = 0;
    }

    public void setGreen() {
        this.status = Boolean.TRUE;
    }

    public void setRed() {
        this.status = Boolean.FALSE;
    }

    public int getId() {
        return id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getTimeToWait() {
        return this.timeToWait;
    }

    public void setTimeToWait(int timeToWait){
        this.timeToWait = timeToWait;
    }

    public int getScoreCars() {
        return scoreCars;
    }

    @Override
    public String toString() {
        return "TrafficLight{" +
                "id=" + id +
                ", streetName='" + streetName + '\'' +
                ", status=" + status +
                ", timeToWait=" + timeToWait +
                '}';
    }

    public void addScoreCar(Integer score) {
        this.scoreCars += score;
        this.nbCars++;
    }

    public int getNbCars() {
        return nbCars;
    }

    public int getPoids() {
       return scoreCars / nbCars;
    }
}
