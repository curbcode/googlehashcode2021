import java.util.ArrayList;
import java.util.List;

public class Intersection {
    private int id;
    List<TrafficLight> trafficLights;

    public Intersection(int id) {
        this.id = id;
        this.trafficLights = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TrafficLight> getTrafficLights() {
        return trafficLights;
    }

    public void setTrafficLights(List<TrafficLight> trafficLights) {
        this.trafficLights = trafficLights;
    }

    @Override
    public String toString() {
        return "Intersection{" +
                "id=" + id +
                ", trafficLights=" + trafficLights +
                '}';
    }


}
