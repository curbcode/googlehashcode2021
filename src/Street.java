public class Street {

    private String name;
    private Integer start;
    private Integer end;
    private Integer duration;

    public Street(final String line) {
        String[] words = line.split(" ");
        this.start = Integer.parseInt(words[0]);
        this.end = Integer.parseInt(words[1]);
        this.name = words[2];
        this.duration = Integer.parseInt(words[3]);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getEnd() {
        return end;
    }

    public void setEnd(Integer end) {
        this.end = end;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Street{" +
                "name='" + name + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", duration=" + duration +
                '}';
    }
}
