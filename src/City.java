import java.util.*;
import java.util.stream.Collectors;

public class City {

    private Integer nbSeconds;
    private Integer nbIntersections;
    private Integer nbStreets;
    private Integer nbCars;
    private Integer nbPoints;
    private final Map<String, Street> streets = new HashMap<>();
    private final List<Car> cars = new ArrayList<>();
    private final Map<Integer, Intersection> intersections = new HashMap<>();

    public City(String line) {
        String[] words = line.split(" ");
        this.nbSeconds = Integer.parseInt(words[0]);
        this.nbIntersections = Integer.parseInt(words[1]);
        this.nbStreets = Integer.parseInt(words[2]);
        this.nbCars = Integer.parseInt(words[3]);
        this.nbPoints = Integer.parseInt(words[4]);
    }

    public Integer getNbSeconds() {
        return nbSeconds;
    }

    public void setNbSeconds(Integer nbSeconds) {
        this.nbSeconds = nbSeconds;
    }

    public Integer getNbIntersections() {
        return nbIntersections;
    }

    public void setNbIntersections(Integer nbIntersections) {
        this.nbIntersections = nbIntersections;
    }

    public Integer getNbStreets() {
        return nbStreets;
    }

    public void setNbStreets(Integer nbStreets) {
        this.nbStreets = nbStreets;
    }

    public Integer getNbCars() {
        return nbCars;
    }

    public void setNbCars(Integer nbCars) {
        this.nbCars = nbCars;
    }

    public Integer getNbPoints() {
        return nbPoints;
    }

    public void setNbPoints(Integer nbPoints) {
        this.nbPoints = nbPoints;
    }

    public Map<String, Street> getStreets() {
        return streets;
    }

    public List<Car> getCars() {
        return cars;
    }

    public Map<Integer, Intersection> getIntersections() {
        return intersections;
    }

    public List<Car> getCarsOrderedByScore() {
        return this.cars.stream()
                .sorted(Comparator.comparingInt(Car::getScore))
                .collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return "City{" +
                "nbSeconds=" + nbSeconds +
                ", nbIntersections=" + nbIntersections +
                ", nbStreets=" + nbStreets +
                ", nbCars=" + nbCars +
                ", nbPoints=" + nbPoints +
                ", streets=" + streets +
                ", cars=" + cars +
                ", intersections=" + intersections +
                '}';
    }
}
