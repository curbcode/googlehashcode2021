import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Reader {


    public static City init_data(String filename) {
        City city = null;
        try {
            File myObj = new File("resources/" + filename);
            Scanner myReader = new Scanner(myObj);
            city = new City(myReader.nextLine());
            Map<Integer, Intersection> intersections = new HashMap<>();
            Integer compteur = 0;
            for (int i = 0; i < city.getNbStreets(); i++){
                String data = myReader.nextLine();
                Street street = new Street(data);
                city.getStreets().put(street.getName(), street);
                String streetName = street.getName();
                // Manage Intersection start
                Intersection intersectionEnd = intersections.get(street.getEnd());
                if (intersectionEnd == null) {
                    intersectionEnd = new Intersection(street.getEnd());
                    intersections.put(street.getEnd(), intersectionEnd);
                }
                TrafficLight trafficLight = new TrafficLight(compteur++, streetName, Boolean.TRUE);
                intersectionEnd.getTrafficLights().add(trafficLight);
            }
            city.getIntersections().putAll(intersections);

            for (int i = 0; i < city.getNbCars(); i++){
                String data = myReader.nextLine();
                Car car = new Car(data, i);
                car.calculateScore(city);
                city.getCars().add(car);

                // Ne pas compter la derniere rue
                for (int j = 0; j < car.getStreets().size() - 1; j++) {
                    String streetName = car.getStreets().get(j);
                    Street street = city.getStreets().get(streetName);
                    List<TrafficLight> tls = city.getIntersections().get(street.getEnd()).getTrafficLights();
                    for(TrafficLight tl: tls) {
                        if (tl.getStreetName().equals(streetName)) {
                            if (car.getScore() != -1) {
                                tl.addScoreCar(car.getScore());
                                continue;
                            }
                        }
                    }
                }
            }

            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return city;

    }


}
