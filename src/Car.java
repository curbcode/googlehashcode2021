import java.util.List;
import java.util.ArrayList;
import java.util.Map;

public class Car {

    private int id;
    private int nbStreets;
    private List<String> streets = new ArrayList<>();
    private Integer score;
    private static final Integer MAUVAIS_SCORE = -1;
    private static final Integer POIDS_TEMPS_PARCOURS = 1;
    private static final Integer POIDS_NB_FEUX = 1;

    public Car(String line, Integer id) {
        this.id = id;
        String[] words = line.split(" ");
        this.nbStreets = Integer.parseInt(words[0]);
        for (int i=1; i<=nbStreets; i++) {
            streets.add(words[i]);
        }
    }

    public void calculateScore(final City city) {
        final Integer tempsSimulation = city.getNbSeconds();
        final Map<String, Street> streets = city.getStreets();
        final Integer tempsParcours = calculerTempsParcours(streets);
        if (tempsParcours > tempsSimulation) {
            this.score = MAUVAIS_SCORE;
        }
        this.score = calculScore(tempsParcours, streets.size());
    }

    private int calculScore(final int tempsParcours, final int nbStreets) {
        // TODO : poids à optimiser
        // TODO : formule à optimiser suivant tempsParcours et nbStreets
        return tempsParcours * POIDS_TEMPS_PARCOURS + nbStreets * POIDS_NB_FEUX;
    }

    private Integer calculerTempsParcours(Map<String, Street> streets) {
        Integer tempsParcours = 0;
        // TODO : à optimiser si possible
        for (String streetName : this.streets) {
            Street street = streets.get(streetName);
            if (streetName.equals(street.getName())) {
                tempsParcours = tempsParcours + street.getDuration();
            }
        }
        return tempsParcours;
    }

    public Integer getScore() {
        return score;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", nbStreets=" + nbStreets +
                ", streets=" + streets +
                ", score=" + score +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNbStreets() {
        return nbStreets;
    }

    public void setNbStreets(int nbStreets) {
        this.nbStreets = nbStreets;
    }

    public List<String> getStreets() {
        return streets;
    }

    public void setStreets(List<String> streets) {
        this.streets = streets;
    }
}
